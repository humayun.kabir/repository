import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../rest/models/repository_response.dart';
import '../../utils/utils.dart';

class RepositoryDetailsScreen extends StatelessWidget {
  RepositoryDetailsScreen({Key? key}) : super(key: key);
  final Repository repository = Get.arguments;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Details of ${repository.fullName}'),
        ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: SizedBox(
                height: 120,
                width: 120,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(25.0),
                  child: FadeInImage.assetNetwork(
                    placeholder: "assets/images/no-photo.jpg",
                    image: repository.owner?.avatarUrl ?? '',
                    imageErrorBuilder: (context, error, stackTrace) {
                      return Image.asset("assets/images/no-photo.jpg");
                    },
                  ),
                ),
              ),
            ),
            const SizedBox(height: 10.0,),
            Center(child: Text('${repository.owner?.login}')),
            Text(repository.description ?? ''),
            Text('Last Update: ${getIsoDateTimeToBD(repository.updatedAt!)}')
          ],
        ),
      ),
    );
  }
}
