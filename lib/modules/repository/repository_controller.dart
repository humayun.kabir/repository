import 'dart:async';

import 'package:brain_station_task/rest/clients/api_client.dart';
import 'package:brain_station_task/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pagination_view/pagination_view.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';
import '../../models/repository_db.dart';
import '../../rest/clients/api_client_provider.dart';
import '../../rest/models/repository_response.dart';
import '../../services/sqlite_service.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class RepositoryController extends GetxController {

  final apiClientProvider = ApiClientProvider();
  final isLoading = false.obs;
  final isOnline = true.obs;

  int repositoryLength = 0; // working on logbookList
  late SqliteService _sqliteService;
  late Database repoDatabase;
  final sortedBy = "".obs;
  List<String> dropDownList = ["Star Count","Last Update"];


  @override
  onInit(){
    // start loading
    isLoading.value = true;
    super.onInit();

    // initialize database
    _sqliteService= SqliteService();
    _sqliteService.initializeDB().then((value) {
      repoDatabase = value;
    }).then((value) {
      // get sort by value from sharedPreference
      getDropDownValue().then((value) {
        // if sort by value wasn't stored in sharedPreference
        // then set sortedBy value to dropDownList first item
        if(value == '') {
          sortedBy.value = dropDownList.first;
        }else{
          sortedBy.value = value;
        }
      });
      // set isOnline value
      checkInternetStatus().then((value) {
        isOnline.value = value;
        // stop loading
        isLoading.value = false;
      });
    });
  }

  // This method is responsible to get sortedBy value from
  // SharedPreference. if there is not valued stored then
  // return '' which is empty string.
  Future<String> getDropDownValue() async {
    final prefs = await SharedPreferences.getInstance();
    String? value = prefs.getString('sortValue');
    return value ?? '';
  }

  // This method is responsible to set/store sortedBy value into
  // SharedPreference.
  void setDropDownValue(String value, GlobalKey<PaginationViewState> paginationKey) async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setString('sortValue',value);
    sortedBy.value = value;
    paginationKey.currentState?.refresh();
  }

  // This method is responsible to return sort String for API call
  String getSortString({bool isForAPI = false}){
    if(sortedBy.value == 'Star Count') {
      return isForAPI ? 'stars' : 'stargazers_count';
    }else if(sortedBy.value == 'Last Update'){
      return isForAPI ? 'updated' : 'updated_at';
    }else{
      return isForAPI ? 'stars' : 'stargazers_count';
    }
  }

  // This function is responsible to call API and get repositoryList
  Future<List<Repository>> getRepository(int offset) async {
    // if app is online, get data from REST API, otherwise get
    // data from Local Database
    if(isOnline.value == false && (offset < repositoryLength || repositoryLength == 0) ) {
      // app is offline, so get data from local database
    List<RepositoryDB> all = await _sqliteService.getRepository(repoDatabase, getSortString());

      List<Repository> newList = all.isNotEmpty
          ? all.map((RepositoryDB e) => repositoryDbToRepository(e)).toList()
          : [];
      repositoryLength = newList.length;
      return newList;
    }else if(isOnline.value == true) {
      // app is online, so get data from REST API
      RepositoryApiClient repositoryApiClient = apiClientProvider.repositoryApiClient;
      // local variable
      List<Repository> loadedSearchResults = [];

      try {
        if (loadedSearchResults.isNotEmpty && offset < loadedSearchResults.length) {
          repositoryLength = loadedSearchResults.length;
          return loadedSearchResults;
        }
        // calculate page from offset
        int page = (offset / 10).ceil() + 1;

        // call REST API
        var serverResponse = await repositoryApiClient.getRepositories({
          'page': page,
          'per_page': 10,
          'sort': getSortString(isForAPI: true),
          'order': 'desc',

        });
        // if REST API response is true
        if (serverResponse.incompleteResults == false) {
          List<Repository> newList = serverResponse.repositoriesList!.isNotEmpty
              ? serverResponse.repositoriesList!
              : [];

          // insert into Database
          newList.forEach((element) {
            _sqliteService.insertRepository(
                repositoryToRepositoryDB(element),
                repoDatabase
            );
          });

          loadedSearchResults.addAll(newList);
          repositoryLength = loadedSearchResults.length;
          return newList;
        } else {
          print('error = ');
          return [];
        }
      } catch (error, stacktrace) {
        print('error catch = $error, stacktrace=$stacktrace');
        return [];
      }
    }else {
      return [];
    }
    }


}