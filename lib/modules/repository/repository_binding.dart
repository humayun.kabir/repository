import 'package:brain_station_task/modules/repository/repository_controller.dart';
import 'package:get/get.dart';

class RepositoryBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RepositoryController());
  }
}
