import 'package:brain_station_task/modules/repository/repository_controller.dart';
import 'package:brain_station_task/utils/widgets/sort_dropdown.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pagination_view/pagination_view.dart';

import '../../rest/models/repository_response.dart';
import '../../utils/widgets/single_repository_card.dart';

class RepositoryScreen extends StatelessWidget {
  RepositoryScreen({Key? key}) : super(key: key);

  final controller = Get.find<RepositoryController>();
  final GlobalKey<PaginationViewState> paginationKey =
      GlobalKey<PaginationViewState>();

  @override
  Widget build(BuildContext context) {
    return Obx(() => controller.isLoading.value == true
        ? const Scaffold(
            body: Center(
              child: CircularProgressIndicator(),
            ),
          )
        : Scaffold(
            appBar: AppBar(
              key: const Key('appBar'),
              title: const Text('Repository List'),
              actions: [
                sortDropdown(
                    dropDownList: controller.dropDownList,
                    selectedValue: controller.sortedBy.value,
                    paginationKey: paginationKey)
              ],
            ),
            body: PaginationView<Repository>(
              preloadedItems: [],
              key: paginationKey,
              paginationViewType: PaginationViewType.listView,
              itemBuilder: (context, repository, index) {
                return Column(
                  children: [
                    singleRepositoryCard(repository, controller, index),
                    if (index <
                        (controller.repositoryLength == null
                            ? -1
                            : controller.repositoryLength! - 1))
                      const SizedBox(height: 10.0),
                  ],
                );
              },
              pageFetch: controller.getRepository,
              pullToRefresh: false,
              onError: (dynamic error) => const Center(
                child: Text('Some error occurred'),
              ),
              onEmpty: const Center(
                child: Text('Sorry! This is empty'),
              ),
              bottomLoader: const Center(
                child: Padding(
                  padding: EdgeInsets.symmetric(vertical: 24.0),
                  child: SizedBox(
                    width: 24.0,
                    height: 24.0,
                    child: CircularProgressIndicator(),
                  ),
                ),
              ),
              initialLoader: const Center(
                child: CircularProgressIndicator(),
              ),
            ),
          ));
  }
}
