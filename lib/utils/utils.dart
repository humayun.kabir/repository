import 'dart:io';

import 'package:brain_station_task/models/repository_db.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

import '../rest/models/repository_response.dart';

String  getIsoDateTimeToBD(String dateTime){
  // convert the ISO8601 DateTime to local DateTime
  DateTime localDate = DateTime.parse(dateTime).toLocal();
  // now format localDate
  return DateFormat("MM-dd-yyyy hh:mm:ss a").format(DateFormat("yyyy-MM-dd HH:mm:ss").parse(localDate.toString()));
}

Repository repositoryDbToRepository(RepositoryDB repositoryDB){
  return Repository(
    id: repositoryDB.id,
    name: repositoryDB.name,
    fullName: repositoryDB.fullName,
    description: repositoryDB.description,
    url: repositoryDB.url,
    starCount: repositoryDB.starCount,
    createdAt: repositoryDB.createdAt,
    updatedAt: repositoryDB.updatedAt,
    pushedAt: repositoryDB.pushedAt,
    owner: Owner(
      id: repositoryDB.ownerId,
      login: repositoryDB.ownerLogin ?? '',
      avatarUrl: repositoryDB.ownerAvatar ?? '',
    )
  );
}

RepositoryDB repositoryToRepositoryDB(Repository repository){
  return RepositoryDB(
      id: repository.id!,
      name: repository.name ?? '',
      fullName: repository.fullName ?? '',
      description: repository.description,
      url: repository.url,
      starCount: repository.starCount ?? 0,
      createdAt: repository.createdAt ?? '',
      updatedAt: repository.updatedAt,
      pushedAt: repository.pushedAt,
      ownerId: repository.owner!.id,
      ownerLogin: repository.owner?.login,
      ownerAvatar: repository.owner?.avatarUrl,
  );
}

Future<bool> checkInternetStatus() async {
  print('start');
  /*try {
    print('start - try');
    final url = Uri.https('google.com');
    var response = await http.head(url);
    if (response.statusCode == 200) {
      return true;
    } else {
      return false;
    }
  } catch (e) {
    return false;
  }*/
  final ConnectivityResult connectivityResult = await (Connectivity().checkConnectivity());
  if (connectivityResult == ConnectivityResult.mobile) {
    print('mobile network');
    return true;
    // Mobile network available.
  } else if (connectivityResult == ConnectivityResult.wifi) {
    // Wi-fi is available.
    // Note for Android:
    // When both mobile and Wi-Fi are turned on system will return Wi-Fi only as active network type
    print('wifi network');
    return true;
  }else if (connectivityResult == ConnectivityResult.none) {
    // No available network types
    print('offline');
    return false;
  }
  return false;
  /*try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      print('connected');
      return true;
    }
  } on SocketException catch (_) {
    print('not connected');
    return false;
  }
  return false;*/
}

void hideKeyboard() {
  var primaryFocus = FocusManager.instance.primaryFocus;
  if (primaryFocus != null) {
    primaryFocus.unfocus();
  }
}

int getAgeFromDateTime(DateTime dateTime){
  int age = 0;
  var today = DateTime.now();
  final difference = today.difference(dateTime).inDays;
  age = difference ~/ 365;
  return age;
}

bool isEmail(String em) {
  String p =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = RegExp(p);
  return regExp.hasMatch(em);
}

  bool isValidPassword(String input) {
/*    r'^
      (?=.*[A-Z])       // should contain at least one upper case
      (?=.*[a-z])       // should contain at least one lower case
      (?=.*?[0-9])      // should contain at least one digit
      (?=.*?[!@#\$&*~]) // should contain at least one Special character
        .{6,}             // Must be at least 8 characters in length
    $*/
    return RegExp(r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{6,}$').hasMatch(input);
  }

