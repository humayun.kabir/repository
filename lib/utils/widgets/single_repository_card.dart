import 'package:brain_station_task/modules/repository/repository_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

import '../../rest/models/repository_response.dart';
import '../../routes/app_routes.dart';
import '../utils.dart';

Widget singleRepositoryCard(Repository repository, RepositoryController controller, int index) {
  return Card(
      elevation: 2.0,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 8.0, 5.0, 5.0),
        child: ListTile(
          //dense: true,
          contentPadding: const EdgeInsets.symmetric(horizontal: 0.0, vertical: 0.0),
          visualDensity: const VisualDensity(horizontal: 0, vertical: -4),

          onTap: () => Get.toNamed(AppRoutes.repositoryDetails, arguments: repository),
          leading: Text('${index+1})'),
          title: Text('${repository.fullName}'),
          subtitle: Row(
            children: [
              Text('Star: ${repository.starCount},'),
              Text(' ${getIsoDateTimeToBD(repository.updatedAt!)}'),

            ],
          ),
          trailing: const Icon(Icons.keyboard_arrow_right),
        ),
      ));
}