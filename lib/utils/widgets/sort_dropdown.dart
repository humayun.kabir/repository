import 'package:brain_station_task/modules/repository/repository_controller.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:pagination_view/pagination_view.dart';

final repositoryController = Get.find<RepositoryController>();

Widget sortDropdown ({
required final List<String> dropDownList,
final TextEditingController? controller,
  final String? selectedValue,
  required final GlobalKey<PaginationViewState> paginationKey,
}){
  //print('selectedValue = for $hintText ${selectedValue}');
  return SizedBox(
    height: 50.0,
    width: 140.0,
    child: DropdownButtonFormField<String>(
      decoration: const InputDecoration(
        contentPadding: EdgeInsets.fromLTRB(10.0, 10.0, 0.0, 0.0),
        errorMaxLines: 1,
        labelStyle: TextStyle(
            fontSize: 14,
            fontFamily: "verdana_regular",
            fontWeight: FontWeight.w400,
            color: Colors.red),
        errorStyle: TextStyle(
          fontSize: 12,
          height: 0.0,
        ),
        fillColor: Colors.transparent,
        filled: true,

      ),
      value: selectedValue != null ? repositoryController.sortedBy.value : null, //appointmentStore.selectedPatientRegistrationId,
      isExpanded: true,
      onChanged: (value){
        controller?.text = value!;
        repositoryController.setDropDownValue(value!, paginationKey);
      },
      validator: (String? value) {
        if (value == null) {
          return "";
        } else {
          return null;
        }
      },
      items: dropDownList.map((dropDownList) {
        return DropdownMenuItem<String>(
          value: dropDownList,
          //child: Text('${localKeyBloodGroup(context,val)}'), //ToDo
          child: Text(dropDownList, style: const TextStyle(color: Colors.black),),
        );
      }).toList(),
      icon: const Padding(
        padding: EdgeInsets.only(right: 10.0),
        child: Icon(
          Icons.expand_more,
          color: Colors.black,
        ),
      ),
    ),
  );
}
