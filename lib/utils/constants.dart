

const baseUrl = 'https://api.github.com/search';
const connectionTimeOut = 120000;
const applicationJson = 'application/json';
const headerAccept = 'Accept';
const headerAuthorization = 'Authorization';