import 'package:brain_station_task/models/repository_db.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class SqliteService {
  Future<Database> initializeDB() async {
    String path = await getDatabasesPath();

    return openDatabase(
      join(path, 'database.db'),
      onCreate: (database, version) async {
        await database.execute(
            "CREATE TABLE Repositories("
                "id INTEGER PRIMARY KEY,"
                "name TEXT NOT NULL,"
                "full_name TEXT NOT NULL,"
                "description TEXT,"
                "url TEXT,"
                "stargazers_count INTEGER NOT NULL,"
                "created_at TEXT NOT NULL,"
                "updated_at TEXT,"
                "pushed_at TEXT,"
                "owner_id INTEGER NOT NULL,"
                "owner_login TEXT,"
                "owner_avatar TEXT"
                ")",
        );
      },
      version: 1,
    );
  }

  Future<int> insertRepository(RepositoryDB repositoryDB, Database db) async {
    final id = await db.insert(
        'Repositories', repositoryDB.toMap(),
        conflictAlgorithm: ConflictAlgorithm.replace);
    return id;
  }

  Future<List<RepositoryDB>> getRepository(Database db, String orderBy) async {
    final List<Map<String, Object?>> queryResult =
    await db.query('Repositories',orderBy: '$orderBy DESC' );
    return queryResult.map((e) => RepositoryDB.fromMap(e)).toList();
  }
}