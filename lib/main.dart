import 'package:brain_station_task/modules/repository/repository_controller.dart';
import 'package:brain_station_task/modules/repository/repository_page.dart';
import 'package:brain_station_task/routes/app_pages.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});
  final repositoryController = Get.put(RepositoryController());

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      key: const Key('GetMaterialApp'),
      debugShowCheckedModeBanner: false,
      title: 'BS Assignment',
      theme: ThemeData(
          useMaterial3: false,
          colorScheme: const ColorScheme(
            brightness: Brightness.light,
            primary: Colors.yellow,
            onPrimary: Colors.black,
            // Colors that are not relevant to AppBar in LIGHT mode:
            //primaryVariant: Colors.grey,
            secondary: Colors.grey,
            //secondaryVariant: Colors.grey,
            onSecondary: Colors.grey,
            background: Colors.grey,
            onBackground: Colors.grey,
            surface: Colors.grey,
            onSurface: Colors.grey,
            error: Colors.grey,
            onError: Colors.grey,
          )),
      initialRoute: '/',
      getPages: AppPages.pages,
      home:  RepositoryScreen(),
    );
  }
}
