import 'package:brain_station_task/modules/repository/repository_binding.dart';
import 'package:brain_station_task/modules/repository/repository_details.dart';
import 'package:get/get.dart';
import 'app_routes.dart';

class AppPages {
  static final List<GetPage> pages = [
    GetPage(
      name: AppRoutes.repositoryDetails,
      page: () => RepositoryDetailsScreen(),
      binding: RepositoryBinding(),
    )
  ];
}
