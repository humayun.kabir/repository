class RepositoryResponse {
  int? totalCount; //total_count
  bool? incompleteResults; // incomplete_results
  List<Repository>? repositoriesList;

  RepositoryResponse({
     this.totalCount,
     this.incompleteResults,
     this.repositoriesList,
  });

  RepositoryResponse.fromJson(Map<String, dynamic> json) {
      totalCount = json['total_count'];
      incompleteResults = json['incomplete_results'];
      if(json['items'] != null){
        repositoriesList =[];
        if(json['items'].isNotEmpty) {
          json['items'].forEach((v) => repositoriesList?.add(Repository.fromJson(v)));
        }
      }
  }
}

class Repository {
  int? id; // id
  String? name; // name
  String? fullName; // full_name
  String? description; // description
  String? url; // url
  int? starCount; // stargazers_count
  String? createdAt; // created_at
  String? updatedAt; // updated_at
  String? pushedAt; // pushed_at
  Owner? owner;

  Repository({
    this.id,
    this.name,
    this.fullName,
    this.description,
    this.url,
    this.starCount,
    this.createdAt,
    this.updatedAt,
    this.pushedAt,
    this.owner,
  });

  factory Repository.fromJson(Map<String, dynamic> json) {
    return Repository(
      id: json['id'],
      name: json['name'],
      fullName: json['full_name'],
      description: json['description'],
      url: json['url'],
      starCount: json['stargazers_count'],
      createdAt: json['created_at'],
      updatedAt: json['updated_at'],
      pushedAt: json['pushed_at'] ,
      owner: json['owner'] != null ? Owner.fromJson(json['owner']) : null,
    );
  }
}

class Owner {
  final int id; // id
  final String login; // login
  final String avatarUrl; // avatar_url


  const Owner({
    required this.id,
    required this.login,
    required this.avatarUrl,
  });

  factory Owner.fromJson(Map<String, dynamic> json) {
    return Owner(
      id: json['id'] as int,
      login: json['login'] as String,
      avatarUrl: json['avatar_url'] as String,
    );
  }
}

