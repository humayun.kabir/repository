import 'package:dio/dio.dart';
import '../../utils/constants.dart';

class AcceptInterceptor extends Interceptor {
  final String value;

  AcceptInterceptor(this.value);

  @override
  Future onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    await addHeaders(options);
    return super.onRequest(options, handler);
  }

  dynamic addHeaders(RequestOptions options) async {
    options.headers.putIfAbsent(headerAccept, () => value);
    return options;
  }
}
