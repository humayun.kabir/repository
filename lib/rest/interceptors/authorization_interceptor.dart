import 'package:dio/dio.dart';
import '../../utils/constants.dart';

class AuthorizationInterceptor extends Interceptor {
  String token = ''; // get token from login
  @override
  Future onRequest(
    RequestOptions options,
    RequestInterceptorHandler handler,
  ) async {
    await addHeaders(options);
    return super.onRequest(options, handler);
  }

  dynamic addHeaders(RequestOptions options) async {

    options.headers.remove(headerAuthorization);
    //if (loginStore.isValidUser) {
      options.headers.putIfAbsent(
          headerAuthorization,
          () => 'Bearer $token');
    //}
    return options;
  }
}
