
import 'package:brain_station_task/utils/constants.dart';
import 'package:dio/dio.dart' hide Headers;
import 'package:retrofit/retrofit.dart';

import '../models/repository_response.dart';

part 'api_client.g.dart';

// flutter pub run build_runner build
// flutter pub run build_runner serve --delete-conflicting-outputs
// flutter pub run build_runner watch


@RestApi(baseUrl: baseUrl)
abstract class RepositoryApiClient {
  factory RepositoryApiClient(Dio dio, {String? baseUrl}) =_RepositoryApiClient;

  @GET('/repositories?q=flutter')
  Future<RepositoryResponse> getRepositories(
      @Queries() Map<String, dynamic> queries,
      );
}



