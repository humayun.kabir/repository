import 'package:brain_station_task/utils/constants.dart';
import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import '../interceptors/accept_interceptor.dart';
import 'api_client.dart';

class ApiClientProvider {
  late RepositoryApiClient repositoryApiClient;

  ApiClientProvider._() {
     _createRepositoryApiClient();
  }

  factory ApiClientProvider() => _instance;

  static final _instance = ApiClientProvider._();

  void _createRepositoryApiClient() {
    var dio = defaultDio;
    dio.options.contentType = applicationJson;
    dio.interceptors.add(AcceptInterceptor(applicationJson));
    repositoryApiClient = RepositoryApiClient(dio);
  }


  Dio get defaultDio {
    var dio = Dio();
    dio.options = BaseOptions(
      connectTimeout: const Duration(milliseconds: connectionTimeOut),
      sendTimeout: const Duration(milliseconds: connectionTimeOut),
      receiveTimeout: const Duration(milliseconds: connectionTimeOut),
      validateStatus: (statusCode){
        if(statusCode == null){
          return false;
        }
        if(statusCode == 400 || statusCode == 403){ // your http status code
          return true;
        }else{
          return statusCode >= 200 && statusCode < 500;
        }
      }
    );
    if (kDebugMode) {
      dio.interceptors.add(
        LogInterceptor(
          requestHeader: true,
          requestBody: true,
          responseBody: true,
        ),
      );
    }
    return dio;
  }
}