class RepositoryDB {
  final int id;
  final String name;
  final String fullName;
  final String? description;
  final String? url;
  final int starCount;
  final String createdAt;
  final String? updatedAt;
  final String? pushedAt;
  final int ownerId;
  final String? ownerLogin;
  final String? ownerAvatar;


  RepositoryDB({
    required this.id,
    required this.name,
    required this.fullName,
    this.description,
    this.url,
    required this.starCount,
    required this.createdAt,
    this.updatedAt,
    this.pushedAt,
    required this.ownerId,
    this.ownerLogin,
    this.ownerAvatar
  });

  RepositoryDB.fromMap(Map<String, dynamic> repository):
      id = repository['id'],
      name = repository['name'],
      fullName = repository['full_name'],
      description = repository['description'],
      url = repository['url'],
      starCount = repository['stargazers_count'],
      createdAt = repository['created_at'],
      updatedAt = repository['updated_at'],
      pushedAt = repository['pushed_at'],
      ownerId = repository['owner_id'],
      ownerLogin = repository['owner_login'],
      ownerAvatar = repository['owner_avatar'];

  Map<String, Object> toMap(){
    return {
      'id':id,
      'name': name,
      'full_name': fullName,
      'description': description ?? '',
      'url': url ?? '',
      'stargazers_count': starCount,
      'created_at': createdAt,
      'updated_at': updatedAt ?? '',
      'pushed_at': pushedAt ?? '',
      'owner_id': ownerId,
      'owner_login': ownerLogin ?? '',
      'owner_avatar': ownerAvatar ?? '',
    };
  }
}