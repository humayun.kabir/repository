// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility in the flutter_test package. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

// flutter drive --driver=integration_test/driver.dart --target=integration_test/app_test.dart
// flutter test integration_test/app_test.dart

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:brain_station_task/main.dart';
//import 'package:sqflite/sqflite.dart';
import 'package:sqflite_common_ffi/sqflite_ffi.dart';
import '../../lib/main.dart' as app;

// flutter test test/widget/widget_test.dart

void main() {
  testWidgets('Repository Widget/smoke test', (WidgetTester tester) async {
    await tester.runAsync(() async {
      // Initialize FFI
      sqfliteFfiInit();
      databaseFactory = databaseFactoryFfi;
      // Build our app and trigger a frame.
      await tester.pumpWidget(MyApp());
      await tester.pump();
    }).then((value) {
      // Verify that app find appBar and appBar Text
      final appBarFinder = find.byKey(const Key('GetMaterialApp'));

      // Use the `findsOneWidget` matcher provided by flutter_test to verify
      // that the Text widgets appear exactly once in the widget tree.
      expect(appBarFinder, findsOneWidget);

    });
  });
}
