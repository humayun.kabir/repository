# Brain Station Assignment

Brain Station 23 Flutter Task by Humayun Kabir

## How to install this project in real device

- clone this repository 
```
git clone git@gitlab.com:humayun.kabir/repository.git
```
- Run pub get command
```
flutter pub get
```
- If you fetch any trouble when running this code into real device, cause may be flutter and dart version mismatch. To remove this issue, delete **android** folder and run below command
```
flutter create .
```
- Hopefully code is running fine.

### What is done in this assignment
- App will open and check internet connection. If connected then call REST API to get data. After getting data from API, will be inserted into Local database for offline use.
- If app is offline, no internet, then app will fetch data from Local Database.
- Sorting works on both fetching data from REST API and Local Database.
- Pagination implemented, each time API call, will fetch 10 data.
- ***SQLite*** database implemented to store repository that was received from REST API.
- ***DIO*** is used as ***http*** client.
- ***Retrofit*** is used as ***DIO*** client.
- ***GetX*** is used for state management.
- ***SharedPreference*** is used for local persistence. _Selected sorting option will persists in future app session._

### Testing
- Widget / UI test written. Open terminal and run this command
```flutter test test/widget/widget_test.dart```